#include <iostream>
#include <opencv2/opencv.hpp>

//this program will display on a window images from a camera

int main(int argc, char* argv[]) 
{
	//matrix declaration
	cv::Mat frame;
	cv::Mat hsv_frame;
	cv::Mat thresholded;
	cv::Mat filtered;
	
	//vector for saving the circles
	std::vector<cv::Vec3f> circles;
	
	//for color detection, hsv min and max values
	cv::Scalar hsv_min = cv::Scalar(10, 140, 200, 0);
	cv::Scalar hsv_max = cv::Scalar(30, 255, 255, 0);
	
	// Create a window and give it a name.
	std::string windowName = "Get video from camera and display it, press any key to exit";
	cv::namedWindow(windowName);
	
	// Initialize video capture from camera and check if it worked.
	//cv::VideoCapture vidCap = cv::VideoCapture("../data/slow_mo2.mp4");
	//cv::VideoCapture vidCap = cv::VideoCapture("../data/orange_circle.jpg");
	cv::VideoCapture vidCap = cv::VideoCapture(0);
	
	if (!vidCap.isOpened()) 
	{
		std::cerr << "Could not open video capture!" << std::endl;
		return 1;
	}
	
	// The main loop.
	while (true) 
	{
		// Read a video frame
		vidCap >> frame;

		if(!frame.empty())
		{
			//convert image to HSV format
			cv::cvtColor(frame, hsv_frame, CV_BGR2HSV, 0);
			
			//orange color detection
			cv::inRange(hsv_frame, hsv_min, hsv_max, thresholded);
			
			//filter noise
			cv::GaussianBlur(thresholded, filtered, cv::Size(9, 9), 2, 2 );
			
			//Apply the Hough Transform to find the circles
			cv::HoughCircles(filtered, circles, CV_HOUGH_GRADIENT, 2, filtered.rows/4, 100, 40, 20, 200);
			
			//Draw the detected circles
			for(size_t i = 0; i < circles.size(); i++)
			{
				cv::Point center(cvRound(circles[i][0]), cvRound(circles[i][1]));
				int radius = cvRound(circles[i][2]);
				
				// circle center
				circle(frame, center, 3, cv::Scalar(0,255,0), -1, 8, 0);
				
				// circle outline
				circle(frame, center, radius, cv::Scalar(0,0,255), 3, 8, 0);
			}
			
			//show image
			cv::imshow(windowName, frame);
		}
		else
		{
			vidCap.set(CV_CAP_PROP_POS_AVI_RATIO , 0);
			std::cout << "Reached end of video, playing again" << std::endl; 
		}

		// Quit the loop when a key is pressed, also give a delay on the video
		int keyPressed = cv::waitKey(1);
		if (keyPressed != -1) break;
	}
	return 0;
}