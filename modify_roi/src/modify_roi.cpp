// Include header files.
#include "cv.h"      // include it to used Main OpenCV functions.
#include "highgui.h" //include it to use GUI functions.
#include "keys.h"

/* 
 * This program takes video from camera, takes a region of interest (ROI)
 * and mutliplies only that region times 0.5, which makes it darker.
 * It is a simple example of how to use cv::Mat::operator and rectangle function
 * to make a ROI.
 * 
 * author: Oscar Lima : olima_84@yahoo.com
 * 
 */

int main(int argc, char* argv[]) 
{
	int option = 0;
	
	//matrix declaration
	cv::Mat frame;
	cv::Mat darker_frame;
	
	// Create a window and give it a name.
	std::string windowName = "Press G to convert to grayscale, N to go back to normal image, ESC to terminate";
	cv::namedWindow(windowName);
	
	// Initialize video capture from camera and check if it worked.
	cv::VideoCapture vidCap = cv::VideoCapture(0);
	
	if (!vidCap.isOpened()) 
	{
		std::cerr << "Could not open video capture!" << std::endl;
		return 1;
	}
	
	//main loop
	while(true)
	{
		// Read a video frame.
		vidCap >> frame;
		
		int key = cv::waitKey(10);
		
		//key = -1 if key was not pressed
		if(key == -1)
		{
			//no key has been pressed
			switch(option)
			{
				case 0:
					// show original color frame
					// Show the image in the window.
					cv::imshow(windowName, frame);
					break;
					
				case 1:
					//defining matrix darker_frame as a pointer subset of frame
					darker_frame = frame.operator()(cv::Rect(50, 50, 540, 380));
					//making the frame darker (reducing light intensity)
					darker_frame = darker_frame * 0.5; //this line modifies "frame"
					// Show the image in the window.
					cv::imshow(windowName, frame);
					break;
				
				default:
					break;
			}
		}
		else if (key == ESC)
		{
			std::cout << "key 'ESC' was pressed, terminating program" << std::endl;
			break;
		}
		else if(key == G)
		{
			std::cout << "key G was pressed : aplying grayscale filter" << std::endl;
			option = 1;
		}
		else if(key == N)
		{
			std::cout << "key N was pressed : showing original color frames" << std::endl;
			option = 0;
		}
		else
		{
			std::cout << "key = " << key << std::endl;
		}
		
	}

	return 0;
}