// Include header files.
#include "cv.h"      // include it to used Main OpenCV functions.
#include "highgui.h" //include it to use GUI functions.
#include "keys.h"

//this program takes the video from the laptop's camera and first
//converts into grayscale, if ESC key is pressed in between it exits the program

int main(int argc, char* argv[]) 
{
	int option = 0;
	
	//grayscale code
	int gray_code = CV_BGR2GRAY;
	
	//matrix to store the grayscale image
	cv::Mat gray_frame;
	
	// Create a window and give it a name.
	std::string windowName = "Press G to convert to grayscale, N to go back to normal image, ESC to terminate";
	cv::namedWindow(windowName);
	
	// Initialize video capture from camera and check if it worked.
	cv::VideoCapture vidCap = cv::VideoCapture(0);
	
	if (!vidCap.isOpened()) 
	{
		std::cerr << "Could not open video capture!" << std::endl;
		return 1;
	}
	
	//main loop
	while(true)
	{
		// Read a video frame.
		cv::Mat frame;
		vidCap >> frame;
		
		int key = cv::waitKey(10);
		
		//key = -1 if key was not pressed
		if(key == -1)
		{
			//no key has been pressed
			switch(option)
			{
				case 0:
					// show original color frame
					// Show the image in the window.
					cv::imshow(windowName, frame);
					break;
					
				case 1:
					//showing grayscale image
					frame = frame * 0.5;
					cv::cvtColor(frame, gray_frame, gray_code); //input image, destination, code
					cv::imshow(windowName, gray_frame);
					break;
				
				default:
					break;
			}
		}
		else if (key == ESC)
		{
			std::cout << "key 'ESC' was pressed, terminating program" << std::endl;
			break;
		}
		else if(key == G)
		{
			std::cout << "key G was pressed : aplying grayscale filter" << std::endl;
			option = 1;
		}
		else if(key == N)
		{
			std::cout << "key N was pressed : showing original color frames" << std::endl;
			option = 0;
		}
		else
		{
			std::cout << "key = " << key << std::endl;
		}
		
		//mandatory call for wait key
		cv::waitKey(1);
		
	}

	return 0;
}