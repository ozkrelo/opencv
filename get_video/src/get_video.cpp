#include <iostream>
#include <opencv2/opencv.hpp>

//this program will display on a window images from a camera

int main(int argc, char* argv[]) 
{
	// Create a window and give it a name.
	std::string windowName = "Get video from camera and display it, press any key to exit";
	cv::namedWindow(windowName);
	
	// Initialize video capture from camera and check if it worked.
	cv::VideoCapture vidCap = cv::VideoCapture(0);
	
	if (!vidCap.isOpened()) 
	{
		std::cerr << "Could not open video capture!" << std::endl;
		return 1;
	}
	
	// The main loop.
	while (true) 
	{
		// Read a video frame.
		cv::Mat frame;
		vidCap >> frame;
		
		// Show the image in the window.
		cv::imshow(windowName, frame);
		
		// Quit the loop when a key is pressed.
		int keyPressed = cv::waitKey(1);
		if (keyPressed != -1) break;
	}
	return 0;
}