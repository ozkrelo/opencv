/*
 * Computer Vision homework 1, Hochschule Bonn-Rhein-Sieg, Germany
 * author : Oscar Lima olima_84@yahoo.com
 * 
 * this program takes frames from the laptops camera, if user press ESC exits, frames
 * are converted to grayscale then a threshold of 64 is applied, all pixels values below 64
 * will be converted to black, the other pixels remain with the same values, then the image
 * is blurred using either a gaussian or median filter (G for gaussian M for median), this last
 * filter is adjustable by the keys + and - to make the frames more or less blurred but it leaves
 * 50 pixel frame unfiltered, press O for original image (unchanged).
 *
 */

#include "cv.h"      // include it to used Main OpenCV functions.
#include "highgui.h" //include it to use GUI functions.
#include "keys.h" //mapping between mac keyboard keys and integers
#define THRESHOLD_VALUE 64.0 //desired threshold of 64

int main(int argc, char* argv[]) 
{
	bool grayscale = false;
	bool gaussian = true;
	bool threshold = false;
	
	int option = 0;
	int ksize = 5;
	
	//matrix to store the grayscale image
	cv::Mat frame;
	cv::Mat gray_frame;
	cv::Mat threshold_frame;
	cv::Mat blurred_frame;
	
	//print instruction usage on terminal
	std::cout << "ESC: exit, Q:toggle grayscale, M:median, G:gaussian, O:original, T:toggle threshold of 64" << std::endl;
	std::cout << "+/- adjust kernel size, " << std::endl;
	
	// Create a window and give it a name.
	std::string windowName = "Q:grayscale, M:median, G:gaussian, O:original, T:threshold, +/-";
	cv::namedWindow(windowName);
	
	// Initialize video capture from camera and check if it worked.
	cv::VideoCapture vidCap = cv::VideoCapture(0);
	
	if (!vidCap.isOpened()) 
	{
		std::cerr << "Could not open video capture!" << std::endl;
		return 1;
	}
	
	//main loop
	while(true)
	{
		// Read a video frame.
		//cv::Mat frame;
		vidCap >> frame;
		
		int key = cv::waitKey(10);
		
		//key = -1 if key was not pressed
		if(key == -1)
		{
			//no key has been pressed
			switch(option)
			{
				case 0:
					// show original color frame
					// Show the image in the window.
					cv::imshow(windowName, frame);
					break;
					
				case 1:
					//process image
					
					//1. applying grayscale if needed
					if(grayscale)
					{
						cv::cvtColor(frame, gray_frame, CV_BGR2GRAY); //input image, destination, code
					}
					else
					{
						gray_frame = frame;
					}
					
					//2. applying threshold of 64 if needed
					if(threshold)
					{
						cv::threshold(gray_frame, threshold_frame, THRESHOLD_VALUE, 0.0, CV_THRESH_TOZERO);
					}
					else
					{
						threshold_frame = gray_frame.clone();
					}
					
					//3. gaussian or median 
					//(adjustable size of kernel, leave border 50 pixels untouched)
					if(gaussian)
					{
						//applying gaussian filter
						cv::GaussianBlur(threshold_frame, blurred_frame, cv::Size(ksize, ksize), 0);
					}
					else
					{
						//applying median filter
						cv::medianBlur(threshold_frame, blurred_frame, ksize);
					}
					
					cv::imshow(windowName, blurred_frame);
					break;
				
				default:
					break;
			}
		}
		else if (key == ESC)
		{
			std::cout << "key 'ESC' was pressed, terminating program" << std::endl;
			break;
		}
		else if(key == O)
		{
			std::cout << "key O was pressed : showing original color frames" << std::endl;
			option = 0;
		}
		else if(key == G)
		{
			std::cout << "key G was pressed : applying gaussian filter" << std::endl;
			option = 1;
			gaussian = true;
		}
		else if(key == M)
		{
			std::cout << "key M was pressed : applying median filter" << std::endl;
			option = 1;
			gaussian = false;
		}
		else if(key == PLUS_SIGN | key == PLUS_SIGN_NUMPAD | key == PLUS_SIGN_EXT_KEYBOARD)
		{
			ksize += 2;
			std::cout << "key + was pressed : increasing kernel size to " << ksize << std::endl;
		}
		else if(key == MINUS_SIGN | key == MINUS_SIGN_NUMPAD)
		{
			ksize -= 2;
			if(ksize < 3)
			{
				ksize = 3;
			}
			
			std::cout << "key - was pressed : reducing kernel size to " << ksize << std::endl;
		}
		else if(key == T)
		{
			std::cout << "key T was pressed : toggle threshold filter" << std::endl;
			option = 1;
			
			if(threshold == true)
			{
				//giving opposite value
				threshold = false;
			}
			else
			{
				threshold = true;
			}
		}
		else if(key == Q)
		{
			//grayscale toggle requested
			std::cout << "key Q was pressed : toggle grayscale filter" << std::endl;
			option = 1;
			
			if(grayscale == true)
			{
				//giving opposite value
				grayscale = false;
			}
			else
			{
				grayscale = true;
			}
		}
		else
		{
			//print the key that was pressed
			std::cout << "key = " << key << std::endl;
		}
	}

	return 0;
}