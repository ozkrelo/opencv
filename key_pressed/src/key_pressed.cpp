// Include header files.
#include "cv.h"      // include it to used Main OpenCV functions.
#include "highgui.h" //include it to use GUI functions.

#define ESC 1048603
#define D 1048676
#define L 1048684
#define R 1048690

int main(int argc, char* argv[]) 
{
	
	//the opencv function waitKey works only if at least one window in open
	//so we need to create a dummy window for testing
	IplImage* img = cvLoadImage( "../data/flower.jpeg" );
	cvNamedWindow("Example1", CV_WINDOW_AUTOSIZE);
	cvShowImage("Example1", img);
	
	while(true)
	{
		int key = cv::waitKey(1000);
		
		//key = -1 if key was not pressed
		if(key != -1)
		{
			std::cout << "##############" << std::endl;
			std::cout << "key = " << key << std::endl;
		}
		else
		{
			std::cout << "The key has not been yet pressed" << std::endl;
		}
		
		if (key == ESC)
		{
			std::cout << "key 'ESC' was pressed, terminating program" << std::endl;
			break;
		}
		else if(key == D)
		{
			std::cout << "key d was pressed" << std::endl;
		}
		else if(key == L)
		{
			std::cout << "key l was pressed" << std::endl;
		}
		else if(key == R)
		{
			std::cout << "key r was pressed" << std::endl;
		}
	}
	
	cvReleaseImage( &img );
	cvDestroyWindow( "Example1" );

	return 0;
}