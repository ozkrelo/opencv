#include<opencv2/opencv.hpp>
#include<cv.h>
#include<opencv2/core/core.hpp>
#include<opencv2/highgui/highgui.hpp>
#include<opencv2/imgproc/imgproc.hpp>
#include<iostream>
#include<stdio.h>

using namespace std;
using namespace cv;

int x=3,y=3;

int main(int, char**)
{
	VideoCapture cap(0);

	if(!cap.isOpened())
	   return -1;

	Mat input, gray, thres, border;
	
	while(1)
	{
		cap >> input;
		//input = imread( "1.jpg", 1 );
		cvtColor(input, gray, CV_BGR2GRAY);
		threshold(gray, thres, 64, 0, 3);
		int flag;
		int key =cv::waitKey(0);

		switch(key)
		{
			case 27:
				exit(1);
				break;
			case 103:
				copyMakeBorder(thres, border, 13, 13, 12, 12, BORDER_REPLICATE);
				GaussianBlur(border, border, Size(x,y), 0, 0);
				flag =1;
				break;
				
			case 109:
				copyMakeBorder(thres, border, 13, 13, 12, 12, BORDER_REPLICATE);
				medianBlur(border, border, x);		
				flag = 2;		
				break;
			
			case 43:
				if(x < 3 || x >= 7)
					continue;
				else
				{
					x = x+2;
					y = y+2;
				}
				break;
			
			case 45:
				if(x <= 3 || x > 7)
					continue;
				else
				{
					x = x-2;
					y = y-2;
				}
				break;
			
			default:
				if(border.empty()) 
					border = thres;
				else
				{
					if(flag == 1)
					{
						copyMakeBorder(thres, border, 13, 13, 12, 12, BORDER_REPLICATE);
						GaussianBlur(border, border, Size(x,y), 0, 0);
				
						imshow("output", border);
					}
					else if(flag == 2)
					{
						copyMakeBorder(thres, border, 13, 13, 12, 12, BORDER_REPLICATE);
						medianBlur(border, border, x);
						imshow("output", border);
					}
				}	
			break;
		}
		
		imshow("Input", input);
		imshow("gray", gray);
	}

    return 0;
}
