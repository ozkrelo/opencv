#include "cv.h"      // include it to used Main OpenCV functions.
#include "highgui.h" //include it to use GUI functions.

//this program will load a jpeg image file and show it on a window

int main(int argc, char** argv)
{
	IplImage* img = cvLoadImage( "../data/flower.jpeg" );
	cvNamedWindow( "Example1", CV_WINDOW_AUTOSIZE );
	cvShowImage("Example1", img);
	
	cvWaitKey(0);
	
	cvReleaseImage( &img );
	cvDestroyWindow( "Example1" );
	
	return 0;
}